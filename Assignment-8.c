/* Wickramaarachchi H.P.
	Index no-19020902*/

//program to collect informations of the students and to print gathered information

#include<stdio.h>
#include<string.h>


//structure to store student data
struct student_details{
	char name[30];
	char subject[20];
	float marks;
};

void main()
{
	
	int i,number;
	
	printf("Enter number of students: ");
	scanf("%d",&number);
	printf("\n");
	
	struct student_details student[5];
	
	printf("Enter details of the students,  \n");
	printf("\n");
	
//loop to get student details
	for(i=1;i<=number;i++)
	{
		printf("Student %d \n",i);
		printf("\n");
		
		printf("\tFirst Name : ");
		scanf("%s",&student[i].name);
		
		printf("\tSubject : ");
		scanf("%s",&student[i].subject);
		
		printf("\tMarks : ");
		scanf("%f",&student[i].marks);
		
		printf("\n");
		
	}
	
	printf("Gathered informations of the students\n");
	printf("\n");
	
//loop to print obtained details
	for(i=1;i<=number;i++)
	{
		printf("Student %d , \n",i);
		printf("\n");
		
		printf("\tFirst Name : %s ",student[i].name);
		printf("\n\tSubject : %s ",student[i].subject);
		printf("\n\tMarks : %.2f \n ",student[i].marks);
		
		printf("\n");
		
	}
}
